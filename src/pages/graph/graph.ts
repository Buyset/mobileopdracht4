import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-graph',
  templateUrl: 'graph.html'
})

export class GraphPage {

  AllExercises: any[] = new Array();
  AllCalories: any[] = new Array();
  labels: any[] = new Array();
  TempArray: any[] = new Array();
  calArray: any[] = new Array();

  constructor(public navCtrl: NavController, private storage: Storage) {
    this.loadData();
  }


  public lineChartData:Array<any> = [
    {data: [], label: 'all calories'}
  ];
  public lineChartLabels:Array<any> = [];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
  
  
  
  // events
  public chartClicked(e:any):void {
    console.log(e);
    this.updateData();
  }

  public chartHovered(e:any):void {
    console.log(e);
    this.updateData();
  }
  


  loadData(){ 

    this.storage.get('testcal').then((val) => {
      if(val!=null)
      {
        this.calArray=val;
        console.log('loaded calarray: ', this.calArray.length);

        for(let i = 0; i < this.calArray.length; i++){
          this.labels[i] = i+1;
        }

        this.lineChartLabels = this.labels;
        this.lineChartData = this.calArray;
      }
    });


  }


  updateData(){ 

    this.storage.get('testcal').then((val) => {
      if(val!=null)
      {
        this.calArray=val;

        for(let i = 0; i < this.calArray.length; i++){
          this.labels[i] = i+1;
        }

        this.lineChartLabels = this.labels;
        this.lineChartData = this.calArray;
      }
    });
      console.log('data updated');
  }
}