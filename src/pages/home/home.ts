import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { controlNameBinding } from '@angular/forms/src/directives/reactive_directives/form_control_name';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
 
    Weight:number[] = new Array(201);
    Hours:number[] = new Array(25);
    Minutes:number[] = new Array(60); 

    stringSplit: any;
    AllExercises: any[] =  new Array();
    TimeArray: any[] = new Array();
    CalArray: any[] = new Array();

    weightInput: number;
    exerciseInput: string;
    hoursInput: any;
    minutesInput: number;
    caloriesPerMinute: number;
    caloriesTotal: number;
    exercise: string;

    totalTime: number;

    hoursSelectOpt: { title: string };
    minutesSelectOpt: { title: string };

    constructor(public navCtrl: NavController, private storage: Storage) {

        for(let i = 0; i < 201; i++){
            this.Weight[i] = i;
        }

        for(let i = 0; i < 25; i++){
            this.Hours[i] = i;
        }

        for(let i = 0; i < 60; i++){
            this.Minutes[i] = i;
        }

        this.hoursSelectOpt = {
            title: 'Hours'
            };

        this.minutesSelectOpt = {
            title: 'Minutes'
            };

        this.loadData();
    }

    loadData(){
        this.storage.get('testcal').then((val) => {
            if(val!=null)
            {
              this.CalArray=val;
              console.log('loaded cals1: ', this.CalArray.length);
            }
        });

    }


    AddExercise(){

        if(this.exerciseInput != null && this.minutesInput != null && this.weightInput != null && this.hoursInput != null){

            this.stringSplit= this.exerciseInput.split('-');
            this.exercise = this.stringSplit[0];
            this.caloriesPerMinute = 0.175 * this.stringSplit[1] * this.weightInput
            this.totalTime = ((this.hoursInput * 60) + this.minutesInput);
            this.caloriesTotal = this.totalTime * this.caloriesPerMinute;

            this.CalArray[this.CalArray.length] = this.caloriesTotal;

            this.storage.set('testcal', this.CalArray);
        }       
    }
}